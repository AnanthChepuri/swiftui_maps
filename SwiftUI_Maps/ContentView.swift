//
//  ContentView.swift
//  SwiftUI_Maps
//
//  Created by Ananth Chepuri on 09/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import SwiftUI
import MapKit

struct ContentView: View {
    @State var checkpoints: [Checkpoint] = [
      Checkpoint(title: "Hyderabad", coordinate: .init(latitude: 17.3850, longitude: 78.4867)),
      Checkpoint(title: "Banglore", coordinate: .init(latitude: 12.9716, longitude: 77.5946))
    ]

    var body: some View {
        MapView(checkpoints: $checkpoints)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
