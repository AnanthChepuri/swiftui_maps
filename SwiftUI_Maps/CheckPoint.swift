//
//  CheckPoint.swift
//  SwiftUI_Maps
//
//  Created by Ananth Chepuri on 09/04/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import MapKit

final class Checkpoint: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    
    init(title: String?, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
    }
}

